use std::fs;

pub fn read_file(name: &str) -> String {
    let f = fs::read_to_string(name);
    f.expect("could not open input file")
}
