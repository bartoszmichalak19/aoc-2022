use aoc_2022::read_file;

enum MoveType {
    Rock,
    Paper,
    Scissors,
}

impl MoveType {
    fn from(s: &str) -> MoveType {
        match s {
            "A" => MoveType::Rock,
            "X" => MoveType::Rock,
            "B" => MoveType::Paper,
            "Y" => MoveType::Paper,
            "C" => MoveType::Scissors,
            "Z" => MoveType::Scissors,
            _ => panic!("wrong move type"),
        }
    }
}

struct Game {
    opponent: MoveType,
    mine: MoveType,
}

fn game_pair(line: &str) -> Game {
    let (opp_str, mine_str) = line.split_once(' ').unwrap();

    Game {
        opponent: MoveType::from(opp_str),
        mine: MoveType::from(mine_str),
    }
}

enum GameResult {
    Win = 6,
    Loose = 0,
    Draw = 3,
}

fn game_result(opponent: MoveType, mine: MoveType) -> GameResult {
    match mine {
        MoveType::Rock => match opponent {
            MoveType::Rock => GameResult::Draw,
            MoveType::Paper => GameResult::Loose,
            MoveType::Scissors => GameResult::Win,
        },
        MoveType::Paper => match opponent {
            MoveType::Rock => GameResult::Win,
            MoveType::Paper => GameResult::Draw,
            MoveType::Scissors => GameResult::Loose,
        },
        MoveType::Scissors => match opponent {
            MoveType::Rock => GameResult::Loose,
            MoveType::Paper => GameResult::Win,
            MoveType::Scissors => GameResult::Draw,
        },
    }
}

fn game_score(opponent: MoveType, mine: MoveType) -> u32 {
    let mut score: u32 = match mine {
        MoveType::Rock => 1,
        MoveType::Paper => 2,
        MoveType::Scissors => 3,
    };
    score += game_result(opponent, mine) as u32;
    score
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn game_score_rock_draw() {
        assert_eq!(4, game_score(MoveType::Rock, MoveType::Rock));
    }

    #[test]
    fn game_result_win() {
        assert_eq!(
            GameResult::Win as u32,
            game_result(MoveType::Paper, MoveType::Scissors) as u32
        );
    }

    #[test]
    fn from_char_to_move_type() {
        assert!(matches!(MoveType::from("B"), MoveType::Paper));
    }

    #[test]
    fn from_str_to_game_pair() {
        let g = game_pair("A Y");
        assert!(matches!(g.opponent, MoveType::Rock));
        assert!(matches!(g.mine, MoveType::Paper));
    }
}

fn main() {
    let input = read_file("inputs/in_day2.txt");

    // answer for the part1
    let total_score: u32 = input
        .lines()
        .into_iter()
        .map(|l| {
            let current_game = game_pair(l);
            game_score(current_game.opponent, current_game.mine)
        })
        .sum();

    println!("Total score is {}", total_score);
}
