use std::fs;

fn main() {
    let s = String::from("inputs/1_1_2021.txt");
    println!("In file {s}");

    let contents = fs::read_to_string(s).expect("File should have been read");
    let mut lines = contents.lines();
    let mut previous: i32 = lines
        .next()
        .expect("Should be a line")
        .parse()
        .expect("Should parse a number");
    let mut depth_inc_counter: u32 = 0;

    for line in lines {
        let current: i32 = line.parse().expect("Should parse a number");
        if current > previous {
            depth_inc_counter += 1;
        }
        previous = current;
    }
    println!(
        "There are {} depth measurement increase events.",
        depth_inc_counter
    )
}
