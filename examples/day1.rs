use aoc_2022::read_file;

fn main() {
    let input = read_file("inputs/in_day1.txt");

    // divide input file into collection of string for each elf backpack
    let elf_backpacks: Vec<&str> = input.split("\n\n").collect();

    // transform backpacks strings into unsigned integers vectors for reach elf
    let elf_backpacks_integers: Vec<Vec<u32>> = elf_backpacks
        .into_iter()
        .map(|str| str.lines().map(|l| l.parse::<u32>().unwrap()).collect())
        .collect();

    // collect vector of sum's of elves backpacks carriage
    let mut elf_backpacks_sum: Vec<u32> = elf_backpacks_integers
        .into_iter()
        .map(|backpack| backpack.iter().sum())
        .collect();

    // find maximum sum of each backpack stuff - answer to part1
    let biggest_glutton_elf_backpack: u32 =
        (&elf_backpacks_sum)
            .into_iter()
            .fold(0, |max, x| if max < *x { *x } else { max });
    println!(
        "Biggest glutton elf carried {} calories in backpack",
        biggest_glutton_elf_backpack
    );

    elf_backpacks_sum.sort();
    elf_backpacks_sum.reverse();
    elf_backpacks_sum.drain(3..);

    // find sum of top three elves carry - answer to part2
    let top_three_elves_carriages_sum: u32 = elf_backpacks_sum.iter().sum();
    println!(
        "Top three glutton elves carry {} calories in total",
        top_three_elves_carriages_sum
    );
}
