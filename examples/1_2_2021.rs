use std::collections::VecDeque;
use std::fs;

fn main() {
    let s = String::from("inputs/1_2_2021.txt");
    println!("In file {s}");

    let contents = fs::read_to_string(s).expect("File should have been read");
    let mut lines = contents.lines();

    let mut window = VecDeque::new();
    for _ in 0..3 {
        window.push_back(
            lines
                .next()
                .expect("Should be a line")
                .parse()
                .expect("Should parse a number"),
        )
    }

    let mut previous_window_sum: i32 = window.iter().sum();
    let mut depth_inc_counter: u32 = 0;

    for line in lines {
        window.pop_front();
        window.push_back(line.parse().expect("Should parse a number"));
        let current_window_sum: i32 = window.iter().sum();
        if current_window_sum > previous_window_sum {
            depth_inc_counter += 1;
        }
        previous_window_sum = current_window_sum;
    }
    println!(
        "There are {} depth measurement increase events.",
        depth_inc_counter
    )
}
