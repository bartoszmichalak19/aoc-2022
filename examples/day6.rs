use aoc_2022::read_file;

fn find_first_marker_pos(window_size: usize, signal: Vec<char>) -> usize {
    let windows_it = signal.windows(window_size);
    let mut first_marker_pos: usize = 0;
    for (i, window) in windows_it.enumerate() {
        let mut v = window.to_vec();
        v.sort();
        v.dedup();
        if v.len() == window_size {
            first_marker_pos = i;
            break;
        };
    }
    first_marker_pos
}

fn main() {
    let input = read_file("inputs/in_day6.txt");
    const PACKET_SIZE: usize = 4;
    const MESSAGE_SIZE: usize = 14;
    println!(
        "Qty of characters until first packet prefix: {}",
        find_first_marker_pos(PACKET_SIZE, input.chars().collect()) + PACKET_SIZE
    );
    println!(
        "Qty of characters until first message prefix: {}",
        find_first_marker_pos(MESSAGE_SIZE, input.chars().collect()) + MESSAGE_SIZE
    );
}

#[cfg(test)]
mod tests {
    use super::*;
}
