use aoc_2022::read_file;
use std::ops::Range;

fn section_boundaries(line: &str) -> Vec<Range<u32>> {
    let elfs_areas: Vec<&str> = line.split(",").collect();
    let mut ranges = Vec::<Range<u32>>::new();
    for elf in elfs_areas {
        let range_limits: Vec<&str> = elf.split("-").collect();
        ranges.push(Range {
            start: range_limits[0].parse::<u32>().unwrap(),
            end: range_limits[1].parse::<u32>().unwrap(),
        });
    }
    ranges
}

fn is_overlapping_part1(mut areas_range: Vec<Range<u32>>) -> bool {
    // sort areas by len with reversed order
    if areas_range[0].len() < areas_range[1].len() {
        areas_range.reverse();
    }

    // check if areas overlaps
    if areas_range[0].start <= areas_range[1].start && areas_range[0].end >= areas_range[1].end {
        true
    } else {
        false
    }
}

fn is_overlapping_part2(mut areas_range: Vec<Range<u32>>) -> bool {
    // sort areas by len with reversed order
    if areas_range[0].len() < areas_range[1].len() {
        areas_range.reverse();
    }

    // check if areas overlaps
    (areas_range[0].start <= areas_range[1].end && areas_range[0].start >= areas_range[1].start)
        || (areas_range[0].end <= areas_range[1].end && areas_range[0].end >= areas_range[1].start)
        || (areas_range[1].start <= areas_range[0].end
            && areas_range[1].start >= areas_range[0].start)
        || (areas_range[1].end <= areas_range[0].end && areas_range[1].end >= areas_range[0].start)
}

fn main() {
    let input = read_file("inputs/in_day4.txt");
    let _duplicated_ranges_count_p1: u32 = input
        .lines()
        .into_iter()
        .filter(|line| is_overlapping_part1(section_boundaries(line)))
        .count() as u32;
    println!(
        "Totally overlapped pairs count: {}",
        _duplicated_ranges_count_p1
    );

    let _duplicated_ranges_count_p2: u32 = input
        .lines()
        .into_iter()
        .filter(|line| is_overlapping_part2(section_boundaries(line)))
        .count() as u32;
    println!("Overlapped pairs count: {}", _duplicated_ranges_count_p2);
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn section_bound_finding() {
        assert_eq!(
            vec![Range { start: 2, end: 4 }, Range { start: 6, end: 8 }],
            section_boundaries("2-4,6-8")
        );
    }

    #[test]
    fn is_overlap() {
        assert_eq!(true, is_overlapping_part1(section_boundaries("2-4,2-8")));
        assert_eq!(false, is_overlapping_part1(section_boundaries("1-3,3-8")));
        assert_eq!(true, is_overlapping_part1(section_boundaries("6-6,4-6")));
    }
}
