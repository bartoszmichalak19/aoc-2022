use aoc_2022::read_file;

fn check_signal(cycle: usize, current_value: i32) -> i32 {
    if cycle == 20 || (cycle + 20) % 40 == 0 {
        let signal = cycle as i32 * current_value;
        return signal;
    }
    return 0;
}

pub struct CathodeRayTube {
    crt_pixels_str: String,
    sprite: Vec<i32>,
}

impl Default for CathodeRayTube {
    fn default() -> CathodeRayTube {
        CathodeRayTube {
            crt_pixels_str: String::new(),
            sprite: vec![0, 1, 2],
        }
    }
}

impl CathodeRayTube {
    pub fn push_signal_sample(&mut self, pos: i32) {
        self.sprite = vec![pos - 1, pos, pos + 1];
        self.draw_pixel();
    }

    fn draw_pixel(&mut self) {
        if self
            .sprite
            .contains(&((self.crt_pixels_str.len() % 40) as i32))
        {
            self.crt_pixels_str.push('#');
        } else {
            self.crt_pixels_str.push('.');
        }
    }

    pub fn display(&mut self) {
        let mut crt_clone = self.crt_pixels_str.clone();
        crt_clone.insert(40, '\n');
        crt_clone.insert(81, '\n');
        crt_clone.insert(122, '\n');
        crt_clone.insert(163, '\n');
        crt_clone.insert(204, '\n');
        crt_clone.insert(245, '\n');

        println!("CRT:\n{}", crt_clone);
    }
}

fn main() {
    let input = read_file("inputs/in_day10.txt");
    let ins: Vec<&str> = input.lines().collect();
    let mut crt = CathodeRayTube::default();
    let mut x = 1;
    let mut cycles: usize = 0;
    let mut _sum_sig_str: i32 = 0;
    for (_i, instruction) in ins.iter().enumerate() {
        if *instruction == "noop" {
            cycles += 1;
            _sum_sig_str += check_signal(cycles, x);
            crt.push_signal_sample(x);
        } else if instruction.starts_with("addx") {
            cycles += 1;
            _sum_sig_str += check_signal(cycles, x);
            crt.push_signal_sample(x);
            cycles += 1;
            let result = instruction.split_once("addx ").unwrap();
            let value = (result.1).parse::<i32>().unwrap();
            _sum_sig_str += check_signal(cycles, x);
            crt.push_signal_sample(x);
            x += value;
        }
    }

    println!("Summary signal strength {}", _sum_sig_str);
    crt.display();
}

#[cfg(test)]
mod tests {
    use super::*;
}
