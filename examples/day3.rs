use aoc_2022::read_file;

fn find_duplicate(line: &str) -> char {
    let (compartment1, compartment2) = line.split_at(line.len() / 2);
    compartment1
        .chars()
        .into_iter()
        .filter(|c| compartment2.contains(*c))
        .next()
        .unwrap()
}

fn find_three_rucksacks_duplicate(three_lines: &[&str]) -> char {
    let (rucksack1, rucksack2, rucksack3) = (three_lines[0], three_lines[1], three_lines[2]);
    rucksack1
        .chars()
        .into_iter()
        .filter(|c| rucksack2.contains(*c))
        .filter(|c| rucksack3.contains(*c))
        .next()
        .unwrap()
}

fn rucksack_duplicate_prio(line: &str) -> u32 {
    const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let mut prio = ALPHABET
        .chars()
        .position(|c| c == find_duplicate(line))
        .unwrap() as u32;
    prio += 1; //alphabet string starts from 0 position
    prio
}

fn rucksack_duplicate_prio_part2(three_lines: &[&str]) -> u32 {
    const ALPHABET: &str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
    let mut prio = ALPHABET
        .chars()
        .position(|c| c == find_three_rucksacks_duplicate(three_lines))
        .unwrap() as u32;
    prio += 1; //alphabet string starts from 0 position
    prio
}

fn main() {
    let input = read_file("inputs/in_day3.txt");

    //answer for the part1
    let duplicates_prio_sum: u32 = input
        .lines()
        .into_iter()
        .map(|l| rucksack_duplicate_prio(l))
        .sum();
    println!("Sum of the priorities {}", duplicates_prio_sum);

    let input = read_file("inputs/in_day3.txt");
    let rucksacks: Vec<&str> = input.split("\n").collect::<Vec<&str>>();
    let triple_rucksacks_groups: Vec<&[&str]> = rucksacks.chunks(3).collect();
    let triple_grouped_rucksacks_sum: u32 = triple_rucksacks_groups
        .into_iter()
        .map(|triple| rucksack_duplicate_prio_part2(triple))
        .sum();

    println!(
        "Sum of priorities of every there rucksacks is {}",
        triple_grouped_rucksacks_sum
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn find_duplicate_from_str() {
        assert_eq!('p', find_duplicate("vJrwpWtwJgWrhcsFMMfFFhFp"));
        assert_eq!('P', find_duplicate("PmmdzqPrVvPwwTWBwg"));
        assert_eq!('v', find_duplicate("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"));
        assert_eq!('t', find_duplicate("ttgJtRGJQctTZtZT"));
        assert_eq!('s', find_duplicate("CrZsJsPPZsGzwwsLwLmpwMDw"));
    }

    #[test]
    fn find_rucksacks_duplicate_str() {
        assert_eq!(
            'r',
            find_three_rucksacks_duplicate(&[
                "vJrwpWtwJgWrhcsFMMfFFhFp",
                "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL",
                "PmmdzqPrVvPwwTWBwg"
            ])
        );
    }

    #[test]
    fn duplicate_priorities() {
        assert_eq!(16, rucksack_duplicate_prio("vJrwpWtwJgWrhcsFMMfFFhFp"));
        assert_eq!(
            38,
            rucksack_duplicate_prio("jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL")
        );
        assert_eq!(42, rucksack_duplicate_prio("PmmdzqPrVvPwwTWBwg"));
        assert_eq!(
            22,
            rucksack_duplicate_prio("wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn")
        );
        assert_eq!(20, rucksack_duplicate_prio("ttgJtRGJQctTZtZT"));
        assert_eq!(19, rucksack_duplicate_prio("CrZsJsPPZsGzwwsLwLmpwMDw"));
    }
}
